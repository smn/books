<?php
require('header.php');
require('api/api.php');

if (isset($_POST['title']) && isset($_POST['author']) && isset($_POST['editor']) && isset($_POST['url']))
{
    $book = new book();
    $book->title = $_POST['title'];
    $book->author = $_POST['author'];
    $book->editor = $_POST['editor'];
    $book->url = $_POST['url'];
    $book->save();
    header('Location: book.php?id=' . $book->id);
}
?>

<html>
 <head>
  <meta http-equiv='Content-Type' content='text/html;charset=utf8'/>
 </head>
 <body>
<?php require('menu.php'); ?>
<form method='post'>
<div>Titre: <input type='text' name='title' /></div>
<div>Auteur: <input type='text' name='author' /></div>
<div>Editeur: <input type='text' name='editor' /></div>
<div>Lien sur le site de l'éditeur: <input type='text' name='url' /></div>
<div><input type='submit' value='Ajouter'/></div>
</form>
</body>
</html>
