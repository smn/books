<?php require('header.php'); ?>
<html>
    <head>
	<meta http-equiv='Content-Type' content='text/html;charset=utf8'/>
    </head>
    <body>
	<?php 
	require('menu.php'); 
	require('api/api.php');
	$id = $_GET['id'];
	$book = book::get($id);
	?>
	<div>
	    <h1><?php echo $book->title; ?></h1>
	</div>
	<div>
	    <li>Auteur : <?php echo $book->author ?></li>
	    <li>Editeur : <?php echo $book->editor ?></li>
	</div>

	<div>
	    <a target='_blank' href='<?php echo $book->url ?>'>Voir ce livre sur le site de l'éditeur</a>
	</div>
	<div>
	    <a target='_blank' href='http://catalogue.bm-lyon.fr/?fn=Search&Style=Portal3&q=<?php echo $book->title; ?>'>Chercher ce livre à la bibliothèque municipale de Lyon</a>
	</div>

	<div>
	    <div>
		<?php
		$reviews = review::bybook($id);
		if (count($reviews) > 0)
		{
			echo 'Fiches de lecture:';
		}
		foreach ($reviews as &$review) 
		{
		    echo '<li>' . $review->user->name . " l'a noté " . $review->rate . "/5 le " . $review->date . '.';
		}
		?>
	    </div>
	    <div>
		<?php
		echo "<a href='addreview.php?bookid=" . $id  . "'>Ajouter une fiche de lecture</a>";
		?>
	    </div>		
	</div>

    </body>
</html>
