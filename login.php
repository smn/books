<?php
require('api/api.php');
if (isset($_POST['login']) && isset($_POST['pw']))
{
	$user = user::auth($_POST['login'], $_POST['pw']);
	if (!is_null($user))
	{
	    session_start();
	    $_SESSION['login'] = $user->login;
            $_SESSION['userid'] = $user->id;
            header('Location: home.php');
	}
	else
	{
	    $error = true;
	}	
}
?>
<html>
 <head>
  <meta http-equiv='Content-Type' content='text/html;charset=utf8'/>
  <title>Identification</title>
 </head>
 <body>
<form method='post'>
<div>Identifiant: <input type='text' name='login' /></div>
<div>Phrase de passe: <input type='password' name='pw' /></div>
<?php
	if ($error)
	{
		echo '<div>Erreur.</div>';
	}
?>
<div><input type='submit' value='S&#39;identifier'/></div>
<div>Pour l'instant, pour créer un compte il faut demander à Simon.</div>
</form>
</body>
</html>
