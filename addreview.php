<?php
require('header.php');
require('api/api.php');

if (isset($_POST['date']) && isset($_POST['rate']))
{
	$date = DateTime::createFromFormat('Y-m-d', $_POST['date']);
	if ($date)
	{
	    session_start();
	    $review = new review();
	    $review->rate = $_POST['rate'];
	    $review->date = $_POST['date'];
	    $review->review = $_POST['review'];
	    $review->book = book::get($_GET['bookid']);
	    $review->user = user::get($_SESSION['userid']);
	    $review->save();
		if ($_POST['sendnotif'] == 'on');
	    {
			$review->sendnotif();
		}
	    header('Location: book.php?id=' . $review->book->id);
	}
	else
	{
		echo "<script>alert('La date doit être au format YYYY-MM-DD')</script>";
	}
}
?>

<html>
    <head>
	<meta http-equiv='Content-Type' content='text/html;charset=utf8'/>
    </head>
    <body>
	<?php require('menu.php');
	$book = book::get($_GET['bookid']);
	echo '<div>Nouvelle fiche de lecture pour ' . $book->title; 
	?>
	<form method='post'>
	    <div>Date de lecture: <input type='date' name='date' /></div>

	    <div>Note: <select name='rate'>
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="-1">Hors catégorie</option>
	    </select></div> 
	    
	    <div>Commentaires: <textarea name='review'></textarea></div>
	    <div>Prévenir les copa.ine.s par courriel: <input type="checkbox" name="sendnotif" checked="true"/></div>

	    <div><input type='submit' value='Ajouter' /></div>
	</form>
    </body>
</html>
