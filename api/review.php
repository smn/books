<?php

class review
{
	var $id;
	var $rate;
	var $date;
	var $review;

	var $user;
	var $book;

	public static function bybook($idbook)
	{
		$recordset = data::get('reviews', 'id,rate,review,date,iduser', 'idbook=' . $idbook, 'date DESC', NULL);
		$result = array();
		while ($record = data::next($recordset))
		{
			$review = new review();
			$review->id = $record['id'];
			$review->rate = $record['rate'];
			$review->date = $record['date'];
			$review->review = $record['review'];
			$review->user = user::get($record['iduser']);
			$review->book = book::get($idbook);
			array_push($result, $review);
		}
		return $result;
	}

    public function save()
    {
        $id = data::put('reviews', $this->id, array(
            "date" => $this->date,
            "rate" => $this->rate,
            "review" => $this->review,
            "idbook" => $this->book->id,
            "iduser" => $this->user->id));
        $this->id = $id;
    }

    public function sendnotif()
    {
    	foreach ($this->user->contacts as &$dest)
		{
			$from = _SMTPFROM;
			$to = $dest;
			$subject = $this->user->name . " a lu '" . html_entity_decode($this->book->title) . "' !";
			$body = "Salut,\n" . 
				$this->user->name . " vient de finir un livre.\n" .
				"Titre: " . html_entity_decode($this->book->title) . "\n" .
				"Auteur: " . html_entity_decode($this->book->author) . "\n" .
				"Editeur: " . html_entity_decode($this->book->editor) . "\n" .
				"Date de fin de lecture: " . $this->date . "\n" .
				"Note: " . $this->rate . "/5";

			if (!empty($this->review))
			{
				$body .= "\nCommentaires: " . $this->review;
			}

			$headers = array(
			        'From' => $from,
			        'To' => $to,
			        'Subject' => $subject
			    );
			$smtp = Mail::factory('smtp', array(
		            'host' => 'ssl://' . _SMTPSERVER,
		            'port' => _SMTPPORT,
		            'auth' => true,
		            'username' => _SMTPLOGIN,
		            'password' => _SMTPPW
			));

			$mail = $smtp->send($to, $headers, $body);

			if (PEAR::isError($mail))
			{
			    die('<p>' . $mail->getMessage() . '</p>');
			}
			else
			{
			    echo('<p>Message successfully sent!</p>');
			}
		}
    }
}

?>
