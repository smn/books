<?php

class data
{    
    static function connect()
    {
        $link = mysql_connect(_DBSERVER, _DBUSER, _DBPW) or die('Impossible de se connecter à la base de données.' );
		mysql_select_db(_DBNAME, $link) or die('Impossible de sélectionner la base de données.');
		mysql_set_charset('utf8');
	}
    
	public static function get($table, $fields, $filter, $order, $limit)
	{
        data::connect();
        
	    $query = 'SELECT ' . $fields . ' FROM bk_' . $table;

        if (isset($filter))
        {
			$query .= ' WHERE ' . $filter; 			
		}   

		if (isset($order))
		{
			$query .= ' ORDER BY ' . $order; 			
		}

		if (isset($limit))
		{
			$query .= ' LIMIT ' . $limit; 			
		}

		$result = mysql_query($query);

		return $result;
	}

    public static function put($table, $id, $fields)
    {
        data::connect();
 
        if (!isset($id))
        {
            $query = "INSERT INTO bk_" . $table . " (id) VALUES (NULL)";
            mysql_query($query) or die ($query . ' ' . mysql_error());
            $result = mysql_query("SELECT LAST_INSERT_ID()");
            $id = mysql_fetch_row($result)[0];
        }
        $list = array();
        foreach ($fields as $name => $value)
        {
	    array_push($list, $name . '=\'' . mysql_real_escape_string(htmlentities($value)) . '\'');
        }
        $query = "UPDATE bk_" . $table . " SET " . implode(',', $list) . " WHERE id=" . $id;
        mysql_query($query) or die ($query . ' ' . mysql_error());
        return $id;
    }

    public static function next($recordset)
	{
		return mysql_fetch_assoc($recordset);
	}

    public static function count($table, $filter)
	{
        data::connect();
        
        $query = 'SELECT count(id) as cnt FROM bk_' . $table . ' WHERE ' . $filter;
		$result = mysql_query($query);
		$row = mysql_fetch_assoc($result);
		return (int) $row['cnt'];
	}
}

?>
