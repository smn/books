<?php

class user
{
    var $login;
    var $name;
    var $id;
    var $email;
    var $contacts;

    public static function auth($login, $password)
    {
	$recordset = data::get('users', 'id,name,login,email,contacts', 'login = \'' . $login . '\' and password = \'' . md5($password) . '\'', NULL, 1);
	$record = data::next($recordset);
        if (isset($record))
        {
            $user = new user();
	    $user->id = $record['id'];
	    $user->login = $record['login'];
	    $user->name = $record['name'];
	    $user->email = $record['email'];
	    $user->contacts = split(",", $record['contacts']);
	    return $user;
	}
	else
	{
	    return NULL;
	}
    }

    public static function get($id)
    {
	$recordset = data::get('users', 'id,name,login,email,contacts', 'id = \'' . $id . '\'', NULL, 1);
	$record = data::next($recordset);
        if (isset($record))
        {
            $user = new user();
	    $user->id = $record['id'];
	    $user->login = $record['login'];
	    $user->name = $record['name'];
	    $user->email = $record['email'];
	    $user->contacts = split (",", $record['contacts']);
	    return $user;
	}
	else
	{
	    return NULL;
	}
    }

}
?>
