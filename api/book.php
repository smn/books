<?php

class book
{
    var $id;
    var $title;
    var $author;
    var $editor;
    var $url;

    function book()
    {
	$this->id = NULL;
    }

    public static function last($count)
    {
	    $recordset = data::get('books', 'id,title', NULL, 'id DESC', $count);
	    $result = array();
	    while ($record = data::next($recordset))
	    {
		    $book = new book();
		    $book->id = $record['id'];
		    $book->title = $record['title'];
		    array_push($result, $book);
	    }
	    return $result;
    }

    public static function get($id)
    {
	    $recordset = data::get('books', 'id,title,author,editor,url', 'id=' . $id, NULL, 1);
	    $record = data::next($recordset);
	    $book = new book();
	    $book->id = $record['id'];
	    $book->title = $record['title'];
	    $book->author = $record['author'];
	    $book->editor = $record['editor'];
	    $book->url = $record['url'];
	    return $book;
    }

    public function save()
    {
	$id = data::put('books', $this->id, array(
	    "title" => $this->title,
	    "author" => $this->author,
	    "editor" => $this->editor,
	    "url" => $this->url));
	$this->id = $id;
    }
}

?>
